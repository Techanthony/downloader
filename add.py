#! /bin/python3

from subprocess import call

VIDEOFILE = "debug"

def addVideo(video, videofile):
	f = open(videofile, 'a')
	video = video + "\n"
	f.write(video)
	f.close()

def countVideos(videofile):
	f = open(videofile, 'r')
	videosList = f.readlines()
	return len(videosList)

call(["clear"])
print("Use quit to end program")
input("press enter:")
call(["clear"])

while True:
	video = input("What video would you like to add: ")
	if str(video) == "quit":
		break
	if str(video) == "num":
		print(countVideos(VIDEOFILE))
		input("Press enter to continue")
	addVideo(video, VIDEOFILE)
	call(["clear"])
